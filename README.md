# SUAI Programming Labs

A repository for learning C, C++ and the basics of Git in SUAI

## Semester 1:
Labs now ready: `7 / 7`
| Lab   | Dev | Ready | Pass | Dop |
| :---- | :-: | :-: | :-: | :-: |
| Lab 0 | ✅ | ✅ | ✅ | ✅ |
| Lab 1 | ✅ | ✅ | ✅ | ✅ |
| Lab 2 | ✅ | ✅ | ✅ | ✅ |
| Lab 3 | ✅ | ✅ | ✅ | ✅ |
| Lab 4 | ✅ | ✅ | ✅ | ✅ |
| Lab 5 | ✅ | ✅ | ✅ | ✅ |
| Lab 6 | ✅ | ✅ | ✅ | ✅ |

## Semester 2:
Labs now ready: `4 / 8`
| Lab   | Dev | Ready | Pass | Dop |
| :---- | :-: | :-: | :-: | :-: |
| Lab 7 | ✅ | ✅ | ✅ | ✅ |
| Lab 8 | 🔷 | 🔷 |  |  |
| Lab 9 | ✅ | ✅ | ✅ | ✅ |
| Lab 10 |  |  |  |  |
| Lab 11 | 🔷 | 🔷 |  |  |
| Lab 12 | 🔷 |  |  |  |
| Lab 13 |  |  |  |  |
| Lab 14 |  |  |  |  |
